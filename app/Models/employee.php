<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class employee extends Model
{
    use HasFactory;
    public $timestamps=false;
    public function getNameAttribute($value)
    {
        return ucfirst($value);
    }
    public function getEmailAttribute($value)
    {
        return $value.'.personal email id';
    }
    public function setNameAttribute($value)
    {
        $this->attributes['name']=$value.".Ms";

    }
    public function getCompany()
    {
        return $this->hasOne('App\Models\company');
    }
    public function getDevice()
    {
        return $this->hasMany('App\Models\device');
    }
}
