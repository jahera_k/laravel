<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\device;

class deleteApi extends Controller
{
    function data($id)
    {
        $device=device::find($id);
        $result=$device->delete();
        if($result)
        {
        return ["$result"=> "deleted"];
        }
        else
        {
            return ["$result"=> "error"];
        }
    }
}
