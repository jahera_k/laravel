<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\db;
use Illuminate\Support\Str;
class EmployeesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        db::table('employees')->insert(
            [
                'name'=>Str::random(10),
                'email'=>Str::random(10).'@gmail.com',
                
            ]
            );
    }
}
