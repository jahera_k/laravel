<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\queryBuilder;
use App\Http\Controllers\aggregateQuery;
use App\Http\Controllers\join;
use App\Http\Controllers\accessor;
use App\Http\Controllers\relationship;
use App\Http\Controllers\onetomany;
use App\Http\Controllers\routeModel;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('query',[queryBuilder::class,'dbOperations']);
Route::get('aggregate',[aggregateQuery::class,'operations']);
Route::get('join',[join::class,'data']);
Route::get('accessor',[accessor::class,'data']);
Route::get('mutator',[mutator::class,'data']);
Route::get('relationship',[relationship::class,'data']);
Route::get('onetomany',[onetomany::class,'data']);
Route::view('string','fluentString');
Route::get('route/{key:name}',[routeModel::class,'data']);


