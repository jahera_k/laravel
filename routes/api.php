<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\firstApi;
use App\Http\Controllers\getDataApi;
use App\Http\Controllers\postApi;
use App\Http\Controllers\putApi;
use App\Http\Controllers\deleteApi;
use App\Http\Controllers\searchApi;
use App\Http\Controllers\validationApi;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('firstapi',[firstApi::class,'data']);
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('getdata',[getDataApi::class,'list']);
Route::post('postdata',[postApi::class,'list']);
Route::put('putdata',[putApi::class,'data']);
Route::delete('delete/{key}',[deleteApi::class,'data']);
Route::get('search/{key}',[searchApi::class,'search']);

